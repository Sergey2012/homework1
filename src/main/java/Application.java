import animals.Action;
import animals.Cat;

public class Application {
    public static void main(String[] args) {
        Cat cat = new Cat("Barsik", "meow", 60, 24.4);
        Cat cat1 = new Cat("Leo","meow", 10,25.6);
        for (String s: cat.liveAnotherDay()) {
            System.out.println(s);
        }

        System.out.println(cat1.getName() + " " + cat.getWeight());
        compareVoice(cat,cat1);
        System.out.println("Всего котиков было создано " + Cat.getCount());
    }
    public static void compareVoice(Cat catVoice, Cat catVoice1){
        if(catVoice.getVoice().equals(catVoice1.getVoice())){
            System.out.println( "Коты одинаково разговаривают" );
        }
        else{
            System.out.println("Коты разговаривают по разному");
        }
    }
}
