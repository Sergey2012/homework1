package util;

import java.util.InputMismatchException;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String scannerYN() {
        String input;
        while (true) {
            input = SCANNER.next();
            if (input.equals("Д") || input.equals("Н"))
                break;
            System.out.println("Ошибка! Неверный ввод! Повторите ввод");
        }
        return input;
    }

    static int scannerInt() {
        String input;
        int inputInt;
        while (true) {
            try {
                input = SCANNER.next();
                inputInt = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println("Ошибка введенно не число");
                continue;
            }
            break;
        }
        return inputInt;
    }

}
