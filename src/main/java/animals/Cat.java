package animals;

import java.util.ArrayList;
import java.util.List;

import static util.TerminalUtil.scannerInt;
import static util.TerminalUtil.scannerYN;

public class Cat extends Action {

    private static final int METHODS = 5;

    private String name;
    private String voice;
    private int satiety;
    private double weight;

    private static int count = 0;

    public Cat(String catName, String catVoice, int catSatiety, double catWeight) {
        name = catName;
        voice = catVoice;
        satiety = catSatiety;
        weight = catWeight;
        count++;
        setCat(this);
    }

    private Cat() {
        count++;
    }

    public static void countCat() {
        System.out.println("Количество созданных котов " + count);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public static int getCount() {
        return count;
    }

    public List<String> liveAnotherDay() {
        List<String> liveArray = new ArrayList<>();
        for (int i = 0; i < 25; i++) {
            int temp = (int) (Math.random() * (METHODS - 1) ) + 1;
            switch (temp) {
                case (1):
                    if (play()) {
                        liveArray.add(i + " играл");
                    }
                     else {
                        System.out.println("Кот голодный. Покормить Д/Н? Сытость:" + getSatiety());
                        if (scannerYN().equals("Д")) {
                            System.out.println("Введите количество еды:");
                            int countEat = scannerInt();
                            System.out.println("Название корма");
                            String nameEat  = scannerYN();
                            eat(countEat, nameEat);
                            liveArray.add(i + " ел");
                        } else {
                            liveArray.add(i + " Покормите кота!");
                        }
                    }

                    break;
                case (2):
                    if (sleep()) {
                        liveArray.add(i + " спал");
                    }
                    else {
                        System.out.println("Кот голодный. Покормить Д/Н ?  Сытость:" + getSatiety());
                        if (scannerYN().equals("Д")) {
                            System.out.println("Введите количество еды:");
                            int countEat = scannerInt();
                            System.out.println("Название корма");
                            String nameEat = scannerYN();
                            eat(countEat, nameEat);
                            liveArray.add(i + " ел");
                        } else {
                            liveArray.add(i + " Покормите кота!");
                        }
                    }
                    break;
                case (3):
                    if (wash()) {
                        liveArray.add(i + " мылся");
                    } else {
                        System.out.println("Кот голодный. Покормить Д/Н? Сытость:" + getSatiety());
                        if (scannerYN().equals("Д")) {
                            System.out.println("Введите количество еды:");
                            int countEat = scannerInt();
                            System.out.println("Название корма");
                            String nameEat  = scannerYN();
                            eat(countEat, nameEat);
                            liveArray.add(i + " ел");
                        } else {
                            liveArray.add(i + " Покормите кота!");
                        }
                        break;
                    }
                case (4):
                    if (walk()) {
                        liveArray.add(i + " гулял");
                    }
                    else {
                        System.out.println("Кот голодный. Покормить Д/Н? Сытость:" + getSatiety());
                        if (scannerYN().equals("Д")) {
                            System.out.println("Введите количество еды:");
                            int countEat = scannerInt();
                            System.out.println("Название корма");
                            String nameEat  = scannerYN();
                            eat(countEat, nameEat);
                            liveArray.add(i + " ел");
                        } else {
                            liveArray.add(i + " Покормите кота!");
                        }
                    }

                    break;
                case (5):
                    if (hunt()) {
                        liveArray.add(i + " охотился");
                    } else {
                        System.out.println("Кот голодный. Покормить Д/Н?? Сытость:" + getSatiety());
                        if (scannerYN().equals("Д")) {
                            System.out.println("Введите количество еды:");
                            int countEat = scannerInt();
                            System.out.println("Название корма");
                            String nameEat  = scannerYN();
                            eat(countEat, nameEat);
                            liveArray.add(i + " ел");
                        } else {
                            liveArray.add(i + " Покормите кота!");
                        }
                        break;
                    }
                default:
                    System.out.println("ERROR! Can't choose an action.");
            }
        }
        return liveArray;
    }
}