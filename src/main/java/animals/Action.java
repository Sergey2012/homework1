package animals;

public class Action {

    private Cat cat;

    public Action() {
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }

    public boolean play() {
        int satiety = cat.getSatiety();
        if (satiety > 5) {
            satiety -= 5;
            cat.setSatiety(satiety);
            return true;
        } else {
            return false;
        }
    }

    public boolean sleep() {
        int satiety = cat.getSatiety();
        if (satiety > 4) {
            satiety -= 4;
            cat.setSatiety(satiety);
            return true;
        } else {
            return false;
        }

    }

    public boolean wash() {
        int satiety = cat.getSatiety();
        if (satiety > 1) {
            satiety -= 1;
            cat.setSatiety(satiety);
            return true;
        } else {
            return false;
        }

    }

    public boolean walk() {
        int satiety = cat.getSatiety();
        if (satiety > 3) {
            satiety -= 3;
            cat.setSatiety(satiety);
            return true;
        } else {
            return false;
        }

    }

    public boolean hunt() {
        int satiety = cat.getSatiety();
        if (satiety > 3) {
            satiety -= 3;
            cat.setSatiety(satiety);
            return true;
        } else {
            return false;
        }
    }

    public void eat(int eda, String nameEat) {
        int satiety = cat.getSatiety();
        satiety += eda;
        cat.setSatiety(satiety);
    }

    public void eat(int eda) {
        int satiety = cat.getSatiety();
        satiety += eda;
        cat.setSatiety(satiety);
    }

    public void eat() {
    }
}
